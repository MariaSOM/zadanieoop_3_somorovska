﻿#include "admin.h"

admin::admin(QWidget * parent) : QDialog(parent) 
{
	ui.setupUi(this);
	
	QFile data("zoznam.txt");
	QMessageBox mbox;
	if (data.open(QFile::ReadOnly))
	{
		QTextStream input(&data);
		while (!input.atEnd())
		{
			QString riadok = input.readLine();
			QString password = input.readLine();
			QString pohlavie = input.readLine();
			QString kredit = input.readLine();
			mena1.push_back(riadok);
			hesla1.push_back(password);
			pohlavia1.push_back(pohlavie);
			kredity1.push_back(kredit);
			
			ui.comboBox->addItem(riadok);
		}
		data.close();
	}
}

admin::~admin()
{	
}

void admin::pridat()
{
	m = ui.meno->text();
	h = ui.heslo->text();
	p = ui.pohlavie->text();

	QFile output("zoznam.txt");
	if (output.open(QFile::Append | QFile::Text))
	{
		QTextStream out(&output);
		out << m << endl;
		out << h << endl;
		out << p << endl;
		out << "0.00" << endl;
		output.close();
	}
	close();
}

void admin::delete1()
{
	zmaz_m = ui.comboBox->currentText();
	int i = ui.comboBox->currentIndex();
	QMessageBox mbox;	

	mena1.removeAt(i);
	hesla1.removeAt(i);
	pohlavia1.removeAt(i);
	kredity1.removeAt(i);

	int pomoc = mena1.size();

	QFile subor("zoznam.txt");
	if (subor.open(QFile::WriteOnly | QFile::Text))
	{
		
		QTextStream f(&subor);
		for(int j = 0; j < pomoc; j++)
		{
				f << mena1[j] << endl;
				f << hesla1[j] << endl;
				f << pohlavia1[j] << endl;
				f << kredity1[j] << endl;
		}
	
	subor.close();	
	}
	close();
}