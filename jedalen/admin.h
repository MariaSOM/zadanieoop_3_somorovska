﻿#pragma once
#include <QDialog>
#include "ui_admin.h"
//#include <QFile>
//#include <QMessegeBox>
#include <heslo.hpp>
#include <jedalen.h>

class admin : public QDialog {
	Q_OBJECT

public:
	admin(QWidget * parent = Q_NULLPTR);
	~admin();

	public slots:
		//void koniec();
		void pridat();
		void delete1();

private:
	Ui::admin ui;

	QString m;
	QString h;
	QString p;
	QString k;
	QString zmaz_m;
	QList<QString> mena1;
	QList<QString> hesla1;
	QList<QString> kredity1;
	QList<QString> pohlavia1;
};
