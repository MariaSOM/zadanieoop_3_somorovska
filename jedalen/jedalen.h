#ifndef JEDALEN_H
#define JEDALEN_H

#include <QtWidgets/QMainWindow>
#include "ui_jedalen.h"
#include <heslo.hpp>
#include <qmessagebox.h>
#include <QString>
#include <qtableview.h>
#include <admin.h>

class jedalen : public QMainWindow
{
	Q_OBJECT

public:
	jedalen(QWidget *parent = 0);
	~jedalen();
	
	void vymazanie_checkBoxov();
	void odcheckovanie_checkBoxov();
	double funkcia(int i);
	void nacitanie_jedal();

	public slots:
		void prihlasenie_klik();
		void odhlasenie_klik();
		void dobi_klik();
		void objednat();
		void zmaz();
		
private:
	Ui::jedalenClass ui;

	int index;
	int pocet_jedal;
	QList<QString> mena;
	QList<QString> hesla;
	QList<QString> kredity;
	QList<QString> pohlavia;

	QList<QString> pomocna;
	QList<QString> jedla;
	QList<QString> cena;
	QList<QString> pomocna1;
	QList<QString> zobrazenie;
	bool stav = false;

};

#endif // JEDALEN_H
