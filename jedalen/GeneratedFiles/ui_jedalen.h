/********************************************************************************
** Form generated from reading UI file 'jedalen.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_JEDALEN_H
#define UI_JEDALEN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_jedalenClass
{
public:
    QWidget *centralWidget;
    QPushButton *prihlasenie;
    QPushButton *pushButton_2;
    QLabel *uzivatel;
    QLabel *kredit;
    QPushButton *pushButton;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *pondelok1;
    QCheckBox *pondelok2;
    QCheckBox *pondelok3;
    QCheckBox *pondelok4;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout_4;
    QCheckBox *utorok1;
    QCheckBox *utorok2;
    QCheckBox *utorok3;
    QCheckBox *utorok4;
    QWidget *tab_3;
    QVBoxLayout *verticalLayout_5;
    QCheckBox *streda1;
    QCheckBox *streda2;
    QCheckBox *streda3;
    QCheckBox *streda4;
    QWidget *tab_4;
    QVBoxLayout *verticalLayout_6;
    QCheckBox *stvrtok1;
    QCheckBox *stvrtok2;
    QCheckBox *stvrtok3;
    QCheckBox *stvrtok4;
    QWidget *tab_5;
    QVBoxLayout *verticalLayout_7;
    QCheckBox *piatok1;
    QCheckBox *piatok2;
    QCheckBox *piatok3;
    QCheckBox *piatok4;
    QWidget *tab_6;
    QHBoxLayout *horizontalLayout_2;
    QListWidget *listWidget;
    QPushButton *pushButton_5;
    QLineEdit *lineEdit;
    QLabel *label;
    QPushButton *pushButton_3;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *jedalenClass)
    {
        if (jedalenClass->objectName().isEmpty())
            jedalenClass->setObjectName(QStringLiteral("jedalenClass"));
        jedalenClass->resize(688, 472);
        centralWidget = new QWidget(jedalenClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        prihlasenie = new QPushButton(centralWidget);
        prihlasenie->setObjectName(QStringLiteral("prihlasenie"));
        prihlasenie->setGeometry(QRect(530, 30, 131, 28));
        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(530, 60, 131, 28));
        uzivatel = new QLabel(centralWidget);
        uzivatel->setObjectName(QStringLiteral("uzivatel"));
        uzivatel->setGeometry(QRect(520, 100, 141, 21));
        kredit = new QLabel(centralWidget);
        kredit->setObjectName(QStringLiteral("kredit"));
        kredit->setGeometry(QRect(520, 130, 121, 21));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(550, 280, 93, 28));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setGeometry(QRect(0, 10, 511, 411));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        verticalLayout_2 = new QVBoxLayout(tab);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        pondelok1 = new QCheckBox(tab);
        pondelok1->setObjectName(QStringLiteral("pondelok1"));

        verticalLayout_2->addWidget(pondelok1);

        pondelok2 = new QCheckBox(tab);
        pondelok2->setObjectName(QStringLiteral("pondelok2"));

        verticalLayout_2->addWidget(pondelok2);

        pondelok3 = new QCheckBox(tab);
        pondelok3->setObjectName(QStringLiteral("pondelok3"));

        verticalLayout_2->addWidget(pondelok3);

        pondelok4 = new QCheckBox(tab);
        pondelok4->setObjectName(QStringLiteral("pondelok4"));

        verticalLayout_2->addWidget(pondelok4);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        verticalLayout_4 = new QVBoxLayout(tab_2);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        utorok1 = new QCheckBox(tab_2);
        utorok1->setObjectName(QStringLiteral("utorok1"));

        verticalLayout_4->addWidget(utorok1);

        utorok2 = new QCheckBox(tab_2);
        utorok2->setObjectName(QStringLiteral("utorok2"));

        verticalLayout_4->addWidget(utorok2);

        utorok3 = new QCheckBox(tab_2);
        utorok3->setObjectName(QStringLiteral("utorok3"));

        verticalLayout_4->addWidget(utorok3);

        utorok4 = new QCheckBox(tab_2);
        utorok4->setObjectName(QStringLiteral("utorok4"));

        verticalLayout_4->addWidget(utorok4);

        tabWidget->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        verticalLayout_5 = new QVBoxLayout(tab_3);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        streda1 = new QCheckBox(tab_3);
        streda1->setObjectName(QStringLiteral("streda1"));

        verticalLayout_5->addWidget(streda1);

        streda2 = new QCheckBox(tab_3);
        streda2->setObjectName(QStringLiteral("streda2"));

        verticalLayout_5->addWidget(streda2);

        streda3 = new QCheckBox(tab_3);
        streda3->setObjectName(QStringLiteral("streda3"));

        verticalLayout_5->addWidget(streda3);

        streda4 = new QCheckBox(tab_3);
        streda4->setObjectName(QStringLiteral("streda4"));

        verticalLayout_5->addWidget(streda4);

        tabWidget->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        verticalLayout_6 = new QVBoxLayout(tab_4);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        stvrtok1 = new QCheckBox(tab_4);
        stvrtok1->setObjectName(QStringLiteral("stvrtok1"));

        verticalLayout_6->addWidget(stvrtok1);

        stvrtok2 = new QCheckBox(tab_4);
        stvrtok2->setObjectName(QStringLiteral("stvrtok2"));

        verticalLayout_6->addWidget(stvrtok2);

        stvrtok3 = new QCheckBox(tab_4);
        stvrtok3->setObjectName(QStringLiteral("stvrtok3"));

        verticalLayout_6->addWidget(stvrtok3);

        stvrtok4 = new QCheckBox(tab_4);
        stvrtok4->setObjectName(QStringLiteral("stvrtok4"));

        verticalLayout_6->addWidget(stvrtok4);

        tabWidget->addTab(tab_4, QString());
        tab_5 = new QWidget();
        tab_5->setObjectName(QStringLiteral("tab_5"));
        verticalLayout_7 = new QVBoxLayout(tab_5);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        piatok1 = new QCheckBox(tab_5);
        piatok1->setObjectName(QStringLiteral("piatok1"));

        verticalLayout_7->addWidget(piatok1);

        piatok2 = new QCheckBox(tab_5);
        piatok2->setObjectName(QStringLiteral("piatok2"));

        verticalLayout_7->addWidget(piatok2);

        piatok3 = new QCheckBox(tab_5);
        piatok3->setObjectName(QStringLiteral("piatok3"));

        verticalLayout_7->addWidget(piatok3);

        piatok4 = new QCheckBox(tab_5);
        piatok4->setObjectName(QStringLiteral("piatok4"));

        verticalLayout_7->addWidget(piatok4);

        tabWidget->addTab(tab_5, QString());
        tab_6 = new QWidget();
        tab_6->setObjectName(QStringLiteral("tab_6"));
        horizontalLayout_2 = new QHBoxLayout(tab_6);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        listWidget = new QListWidget(tab_6);
        listWidget->setObjectName(QStringLiteral("listWidget"));

        horizontalLayout_2->addWidget(listWidget);

        pushButton_5 = new QPushButton(tab_6);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));

        horizontalLayout_2->addWidget(pushButton_5);

        tabWidget->addTab(tab_6, QString());
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(522, 250, 141, 22));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(530, 170, 141, 61));
        pushButton_3 = new QPushButton(centralWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setGeometry(QRect(550, 310, 93, 28));
        jedalenClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(jedalenClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 688, 26));
        jedalenClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(jedalenClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        jedalenClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(jedalenClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        jedalenClass->setStatusBar(statusBar);

        retranslateUi(jedalenClass);
        QObject::connect(prihlasenie, SIGNAL(clicked()), jedalenClass, SLOT(prihlasenie_klik()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), jedalenClass, SLOT(odhlasenie_klik()));
        QObject::connect(pushButton, SIGNAL(clicked()), jedalenClass, SLOT(dobi_klik()));
        QObject::connect(piatok1, SIGNAL(clicked()), jedalenClass, SLOT(pia1_checked()));
        QObject::connect(piatok2, SIGNAL(clicked()), jedalenClass, SLOT(pia2_checked()));
        QObject::connect(piatok3, SIGNAL(clicked()), jedalenClass, SLOT(pia3_checked()));
        QObject::connect(piatok4, SIGNAL(clicked()), jedalenClass, SLOT(pia4_checked()));
        QObject::connect(pondelok1, SIGNAL(clicked()), jedalenClass, SLOT(po1_checked()));
        QObject::connect(pondelok2, SIGNAL(clicked()), jedalenClass, SLOT(po2_checked()));
        QObject::connect(pondelok3, SIGNAL(clicked()), jedalenClass, SLOT(po3_checked()));
        QObject::connect(pondelok4, SIGNAL(clicked()), jedalenClass, SLOT(po4_checked()));
        QObject::connect(utorok1, SIGNAL(clicked()), jedalenClass, SLOT(ut1_checked()));
        QObject::connect(utorok2, SIGNAL(clicked()), jedalenClass, SLOT(ut2_checked()));
        QObject::connect(utorok3, SIGNAL(clicked()), jedalenClass, SLOT(ut3_checked()));
        QObject::connect(utorok4, SIGNAL(clicked()), jedalenClass, SLOT(ut4_checked()));
        QObject::connect(streda1, SIGNAL(clicked()), jedalenClass, SLOT(str1_checked()));
        QObject::connect(streda2, SIGNAL(clicked()), jedalenClass, SLOT(str2_checked()));
        QObject::connect(streda3, SIGNAL(clicked()), jedalenClass, SLOT(str3_checked()));
        QObject::connect(streda4, SIGNAL(clicked()), jedalenClass, SLOT(str4_checked()));
        QObject::connect(stvrtok1, SIGNAL(clicked()), jedalenClass, SLOT(stv1_checked()));
        QObject::connect(stvrtok2, SIGNAL(clicked()), jedalenClass, SLOT(stv2_checked()));
        QObject::connect(stvrtok3, SIGNAL(clicked()), jedalenClass, SLOT(stv3_checked()));
        QObject::connect(stvrtok4, SIGNAL(clicked()), jedalenClass, SLOT(stv4_checked()));
        QObject::connect(pushButton_3, SIGNAL(clicked()), jedalenClass, SLOT(objednat()));
        QObject::connect(pushButton_5, SIGNAL(clicked()), jedalenClass, SLOT(zmaz()));
        QObject::connect(listWidget, SIGNAL(activated(QModelIndex)), jedalenClass, SLOT(zmaz()));

        tabWidget->setCurrentIndex(5);


        QMetaObject::connectSlotsByName(jedalenClass);
    } // setupUi

    void retranslateUi(QMainWindow *jedalenClass)
    {
        jedalenClass->setWindowTitle(QApplication::translate("jedalenClass", "jedalen", 0));
        prihlasenie->setText(QApplication::translate("jedalenClass", "Prihlasenie", 0));
        pushButton_2->setText(QApplication::translate("jedalenClass", "Odhlasenie", 0));
        uzivatel->setText(QApplication::translate("jedalenClass", "pouzivatel:", 0));
        kredit->setText(QApplication::translate("jedalenClass", "kredit: ", 0));
        pushButton->setText(QApplication::translate("jedalenClass", "dobit kredit", 0));
        pondelok1->setText(QApplication::translate("jedalenClass", "CheckBox", 0));
        pondelok2->setText(QApplication::translate("jedalenClass", "CheckBox", 0));
        pondelok3->setText(QApplication::translate("jedalenClass", "CheckBox", 0));
        pondelok4->setText(QApplication::translate("jedalenClass", "CheckBox", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("jedalenClass", "Pondelok", 0));
        utorok1->setText(QApplication::translate("jedalenClass", "CheckBox", 0));
        utorok2->setText(QApplication::translate("jedalenClass", "CheckBox", 0));
        utorok3->setText(QApplication::translate("jedalenClass", "CheckBox", 0));
        utorok4->setText(QApplication::translate("jedalenClass", "CheckBox", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("jedalenClass", "Utorok", 0));
        streda1->setText(QApplication::translate("jedalenClass", "CheckBox", 0));
        streda2->setText(QApplication::translate("jedalenClass", "CheckBox", 0));
        streda3->setText(QApplication::translate("jedalenClass", "CheckBox", 0));
        streda4->setText(QApplication::translate("jedalenClass", "CheckBox", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("jedalenClass", "Streda", 0));
        stvrtok1->setText(QApplication::translate("jedalenClass", "CheckBox", 0));
        stvrtok2->setText(QApplication::translate("jedalenClass", "CheckBox", 0));
        stvrtok3->setText(QApplication::translate("jedalenClass", "CheckBox", 0));
        stvrtok4->setText(QApplication::translate("jedalenClass", "CheckBox", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QApplication::translate("jedalenClass", "Stvrtok", 0));
        piatok1->setText(QApplication::translate("jedalenClass", "CheckBox", 0));
        piatok2->setText(QApplication::translate("jedalenClass", "CheckBox", 0));
        piatok3->setText(QApplication::translate("jedalenClass", "CheckBox", 0));
        piatok4->setText(QApplication::translate("jedalenClass", "CheckBox", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_5), QApplication::translate("jedalenClass", "Piatok", 0));
        pushButton_5->setText(QApplication::translate("jedalenClass", "Zmazat ", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_6), QApplication::translate("jedalenClass", "Moje obedy", 0));
        label->setText(QString());
        pushButton_3->setText(QApplication::translate("jedalenClass", "Objednat", 0));
    } // retranslateUi

};

namespace Ui {
    class jedalenClass: public Ui_jedalenClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_JEDALEN_H
