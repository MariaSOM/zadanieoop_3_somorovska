/********************************************************************************
** Form generated from reading UI file 'heslo.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HESLO_H
#define UI_HESLO_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_heslo
{
public:
    QLabel *l_meno;
    QLineEdit *zadaj_heslo;
    QLabel *l_heslo;
    QPushButton *button_nacitaj;
    QComboBox *comboBox;

    void setupUi(QDialog *heslo)
    {
        if (heslo->objectName().isEmpty())
            heslo->setObjectName(QStringLiteral("heslo"));
        heslo->resize(363, 75);
        l_meno = new QLabel(heslo);
        l_meno->setObjectName(QStringLiteral("l_meno"));
        l_meno->setGeometry(QRect(20, 10, 55, 16));
        zadaj_heslo = new QLineEdit(heslo);
        zadaj_heslo->setObjectName(QStringLiteral("zadaj_heslo"));
        zadaj_heslo->setGeometry(QRect(160, 30, 131, 22));
        zadaj_heslo->setEchoMode(QLineEdit::Password);
        l_heslo = new QLabel(heslo);
        l_heslo->setObjectName(QStringLiteral("l_heslo"));
        l_heslo->setGeometry(QRect(180, 10, 55, 16));
        button_nacitaj = new QPushButton(heslo);
        button_nacitaj->setObjectName(QStringLiteral("button_nacitaj"));
        button_nacitaj->setGeometry(QRect(300, 20, 51, 41));
        comboBox = new QComboBox(heslo);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(10, 30, 131, 22));

        retranslateUi(heslo);
        QObject::connect(button_nacitaj, SIGNAL(clicked()), heslo, SLOT(klik_ok()));
        QObject::connect(comboBox, SIGNAL(activated(QString)), heslo, SLOT(nacitaj()));

        QMetaObject::connectSlotsByName(heslo);
    } // setupUi

    void retranslateUi(QDialog *heslo)
    {
        heslo->setWindowTitle(QApplication::translate("heslo", "heslo", 0));
        l_meno->setText(QApplication::translate("heslo", "meno", 0));
        l_heslo->setText(QApplication::translate("heslo", "heslo", 0));
        button_nacitaj->setText(QApplication::translate("heslo", "OK", 0));
    } // retranslateUi

};

namespace Ui {
    class heslo: public Ui_heslo {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HESLO_H
