/********************************************************************************
** Form generated from reading UI file 'admin.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADMIN_H
#define UI_ADMIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_admin
{
public:
    QComboBox *comboBox;
    QPushButton *pushButton;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLineEdit *meno;
    QLineEdit *heslo;
    QLineEdit *pohlavie;
    QPushButton *pushButton_2;

    void setupUi(QWidget *admin)
    {
        if (admin->objectName().isEmpty())
            admin->setObjectName(QStringLiteral("admin"));
        admin->resize(339, 250);
        comboBox = new QComboBox(admin);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(20, 40, 201, 31));
        pushButton = new QPushButton(admin);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(230, 40, 93, 28));
        label = new QLabel(admin);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 10, 311, 16));
        label_2 = new QLabel(admin);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(30, 90, 201, 16));
        label_3 = new QLabel(admin);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(30, 120, 55, 16));
        label_4 = new QLabel(admin);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(30, 150, 55, 16));
        label_5 = new QLabel(admin);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(30, 180, 55, 16));
        meno = new QLineEdit(admin);
        meno->setObjectName(QStringLiteral("meno"));
        meno->setGeometry(QRect(180, 120, 113, 22));
        heslo = new QLineEdit(admin);
        heslo->setObjectName(QStringLiteral("heslo"));
        heslo->setGeometry(QRect(180, 150, 113, 22));
        pohlavie = new QLineEdit(admin);
        pohlavie->setObjectName(QStringLiteral("pohlavie"));
        pohlavie->setGeometry(QRect(180, 180, 113, 22));
        pushButton_2 = new QPushButton(admin);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(190, 210, 93, 28));

        retranslateUi(admin);
        QObject::connect(pushButton, SIGNAL(clicked()), admin, SLOT(delete1()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), admin, SLOT(pridat()));
        QObject::connect(comboBox, SIGNAL(activated(QString)), admin, SLOT(load()));

        QMetaObject::connectSlotsByName(admin);
    } // setupUi

    void retranslateUi(QWidget *admin)
    {
        admin->setWindowTitle(QApplication::translate("admin", "admin", 0));
        pushButton->setText(QApplication::translate("admin", "Zmazat", 0));
        label->setText(QApplication::translate("admin", "Zadajte meno uzivatela ktoreho chcete zmazat", 0));
        label_2->setText(QApplication::translate("admin", "Zadajte udaje o novom uzivatelovi", 0));
        label_3->setText(QApplication::translate("admin", "meno:", 0));
        label_4->setText(QApplication::translate("admin", "heslo:", 0));
        label_5->setText(QApplication::translate("admin", "pohlavie:", 0));
        pushButton_2->setText(QApplication::translate("admin", "Pridat", 0));
    } // retranslateUi

};

namespace Ui {
    class admin: public Ui_admin {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADMIN_H
