#ifndef HESLO_HPP
#define HESLO_HPP

#include <QDialog>
#include "ui_heslo.h"
#include <vector>
#include <QFile>
#include <QTextStream>
#include <QFileDialog>
#include <qcombobox.h>
#include <QWidget>
#include <qmessagebox.h>
#include <qlineedit.h>
#include <qfile.h>


class heslo : public QDialog
{
	Q_OBJECT

public:
	heslo(QWidget *parent = 0);
	~heslo();
	
	QString DajMeno();
	QString DajHeslo();
	
	public slots:
		void klik_ok();

private:
	Ui::heslo ui;

	QString meno;
	QString heslo1;
};

#endif // HESLO_HPP
