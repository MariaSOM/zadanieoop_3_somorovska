#include "heslo.hpp"

heslo::heslo(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint);

	QFile data("zoznam.txt");
	QMessageBox mbox;
	if (data.open(QFile::ReadOnly))
	{
		QTextStream input(&data);
		while (!input.atEnd())
		{
			QString riadok = input.readLine();
			QString password = input.readLine();
			QString pohlavie = input.readLine();
			QString kredit = input.readLine();
			ui.comboBox->addItem(riadok);
		}
		data.close();
	}
}

heslo::~heslo()
{

}

QString heslo::DajMeno()
{
	return meno;
}

QString heslo::DajHeslo()
{
	return heslo1;
}

void heslo::klik_ok()
{	
		meno = ui.comboBox->currentText();
		heslo1 = ui.zadaj_heslo->text();
		close();	
}