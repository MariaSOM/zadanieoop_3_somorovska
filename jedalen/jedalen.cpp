#include "jedalen.h"

jedalen::jedalen(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	QFile data("zoznam.txt");
	QMessageBox mbox;
	if (data.open(QFile::ReadOnly))
	{
		QTextStream input(&data);
		while (!input.atEnd())
		{
			QString riadok = input.readLine();
			QString password = input.readLine();
			QString pohlavie = input.readLine();
			QString kredit = input.readLine();
			mena.push_back(riadok);
			hesla.push_back(password);
			pohlavia.push_back(pohlavie);
			kredity.push_back(kredit);
		}
		data.close();
	}

	QFile data1("jedla.txt");
	if (data1.open(QFile::ReadOnly))
	{
		QTextStream input(&data1);
		while (!input.atEnd())
		{
			QString food = input.readLine();
			QString price = input.readLine();
			jedla.push_back(food);
			cena.push_back(price);
		}
	}
	data1.close();

	vymazanie_checkBoxov();
	ui.lineEdit->setValidator(new QDoubleValidator(0, 100, 2, this));
}

jedalen::~jedalen()
{

}

void jedalen::odcheckovanie_checkBoxov()
{
	ui.pondelok1->setChecked(false);
	ui.pondelok2->setChecked(false);
	ui.pondelok3->setChecked(false);
	ui.pondelok4->setChecked(false);

	ui.utorok1->setChecked(false);
	ui.utorok2->setChecked(false);
	ui.utorok3->setChecked(false);
	ui.utorok4->setChecked(false);

	ui.streda1->setChecked(false);
	ui.streda2->setChecked(false);
	ui.streda3->setChecked(false);
	ui.streda4->setChecked(false);

	ui.stvrtok1->setChecked(false);
	ui.stvrtok2->setChecked(false);
	ui.stvrtok3->setChecked(false);
	ui.stvrtok4->setChecked(false);

	ui.piatok1->setChecked(false);
	ui.piatok2->setChecked(false);
	ui.piatok3->setChecked(false);
	ui.piatok4->setChecked(false);
}

void jedalen::vymazanie_checkBoxov()
{
	ui.uzivatel->setText("pouzivatel: ");
	ui.kredit->setText("kredit: ");

	ui.pondelok1->setText("");
	ui.pondelok2->setText(""); 
	ui.pondelok3->setText("");
	ui.pondelok4->setText("");
	
	ui.utorok1->setText("");
	ui.utorok2->setText("");
	ui.utorok3->setText("");
	ui.utorok4->setText("");
	
	ui.streda1->setText(""); 
	ui.streda2->setText("");
	ui.streda3->setText("");
	ui.streda4->setText("");
	
	ui.stvrtok1->setText("");
	ui.stvrtok2->setText("");
	ui.stvrtok3->setText("");
	ui.stvrtok4->setText("");
	
	ui.piatok1->setText("");
	ui.piatok2->setText("");
	ui.piatok3->setText("");
	ui.piatok4->setText("");
	
	ui.label->setText("Pre zobrazenie obedov\n musite byt prihlaseny!");
}

double jedalen::funkcia(int i)
{
	QMessageBox mbox;
	double zostatok = kredity[index].toDouble() - cena[i].toDouble();
	if (zostatok < 0)
	{
		mbox.setText("nemate dostatocny kredit.");
		zostatok = kredity[index].toDouble();
		mbox.exec();
	}
	else
	{
		QFile output(mena[index] + ".txt");
		if (output.open(QFile::Append | QFile::Text))
		{
			QTextStream out(&output);
			out << jedla[i] << endl;
			output.close();
		}

		int n = 0;

		QFile input("zoznam.txt");
		if (input.open(QFile::ReadOnly | QFile::Text))
		{
			QTextStream in(&input);
			while (!input.atEnd())
			{
				QString riadok = input.readLine();
				pomocna1.push_back(riadok);
				n++;
			}

			input.close();

			for (int i = 0; i < n - 3; i++)
			{
				int x = QString::compare(pomocna1[i], mena[index], Qt::CaseSensitive);
				if (x == 1)
				{
					pomocna1[i + 3] = (QString::number(zostatok) + "\n");
					ui.kredit->setText("kredit: " + QString::number(zostatok));
				}
			}

			QFile output1("zoznam.txt");
			if (output1.open(QFile::WriteOnly | QFile::Text))
			{
				QTextStream out1(&output1);
				for (int i = 0; i < n; i++)
					out1 << pomocna1[i];
				output1.close();
			}
			kredity[index] = QString::number(zostatok);
		}
	}
	return zostatok;
}

void jedalen::nacitanie_jedal()
{
	ui.listWidget->clear();
	zobrazenie.clear();

	QFile data(mena[index] + ".txt");
	if (data.open(QFile::ReadOnly))
	{
		QTextStream input(&data);
		while (!input.atEnd())
		{
			QString riadok = input.readLine();
			zobrazenie.push_back(riadok);
			ui.listWidget->addItem(riadok);
		}
		data.close();
	}
}

void jedalen::prihlasenie_klik()
{
	heslo prihlasenie;
	QMessageBox mbox;
	prihlasenie.exec();
	index = mena.indexOf(prihlasenie.DajMeno());
	QString text = QString::number(index);

	if (index == -1 || prihlasenie.DajHeslo() != hesla[index])
		stav = false;
	else
	stav = true;

	if (stav == true)
	{
		if (mena[index] == "admin")
		{
			admin ad;
			ad.exec();	
		}
		else
		{
			ui.uzivatel->setText("pouzivatel: " + mena[index]);
			ui.kredit->setText("kredit: " + kredity[index]);
			ui.label->setText("");

			ui.pondelok1->setText(jedla[0] + "\n cena: " + cena[0]);
			ui.pondelok2->setText(jedla[1] + "\n cena: " + cena[1]);
			ui.pondelok3->setText(jedla[2] + "\n cena: " + cena[2]);
			ui.pondelok4->setText(jedla[3] + "\n cena: " + cena[3]);

			ui.utorok1->setText(jedla[4] + "\n cena: " + cena[4]);
			ui.utorok2->setText(jedla[5] + "\n cena: " + cena[5]);
			ui.utorok3->setText(jedla[6] + "\n cena: " + cena[6]);
			ui.utorok4->setText(jedla[7] + "\n cena: " + cena[7]);

			ui.streda1->setText(jedla[8] + "\n cena: " + cena[8]);
			ui.streda2->setText(jedla[9] + "\n cena: " + cena[9]);
			ui.streda3->setText(jedla[10] + "\n cena: " + cena[10]);
			ui.streda4->setText(jedla[11] + "\n cena: " + cena[11]);

			ui.stvrtok1->setText(jedla[12] + "\n cena: " + cena[12]);
			ui.stvrtok2->setText(jedla[13] + "\n cena: " + cena[13]);
			ui.stvrtok3->setText(jedla[14] + "\n cena: " + cena[14]);
			ui.stvrtok4->setText(jedla[15] + "\n cena: " + cena[15]);

			ui.piatok1->setText(jedla[16] + "\n cena: " + cena[16]);
			ui.piatok2->setText(jedla[17] + "\n cena: " + cena[17]);
			ui.piatok3->setText(jedla[18] + "\n cena: " + cena[18]);
			ui.piatok4->setText(jedla[19] + "\n cena: " + cena[19]);

			nacitanie_jedal();
		}
	}
	else
	{
		vymazanie_checkBoxov();
		mbox.setText("Zle meno alebo heslo pouzivatela!");
		mbox.exec();
	}
}

void jedalen::objednat()
{
	
	if (stav == true)
	{
		double zostatok;
		if (ui.pondelok1->isChecked())
			zostatok = funkcia(0);
		if (ui.pondelok2->isChecked())
			zostatok = funkcia(1);
		if (ui.pondelok3->isChecked())
			zostatok = funkcia(2);
		if (ui.pondelok4->isChecked())
			zostatok = funkcia(3);
		if (ui.utorok1->isChecked())
			zostatok = funkcia(4);
		if (ui.utorok2->isChecked())
			zostatok = funkcia(5);
		if (ui.utorok3->isChecked())
			zostatok = funkcia(6);
		if (ui.utorok4->isChecked())
			zostatok = funkcia(7);
		if (ui.streda1->isChecked())
			zostatok = funkcia(8);
		if (ui.streda2->isChecked())
			zostatok = funkcia(9);
		if (ui.streda3->isChecked())
			zostatok = funkcia(10);
		if (ui.streda4->isChecked())
			zostatok = funkcia(11);
		if (ui.stvrtok1->isChecked())
			zostatok = funkcia(12);
		if (ui.stvrtok2->isChecked())
			zostatok = funkcia(13);
		if (ui.stvrtok3->isChecked())
			zostatok = funkcia(14);
		if (ui.stvrtok4->isChecked())
			zostatok = funkcia(15);
		if (ui.piatok1->isChecked())
			zostatok = funkcia(16);
		if (ui.piatok2->isChecked())
			zostatok = funkcia(17);
		if (ui.piatok3->isChecked())
			zostatok = funkcia(18);
		if (ui.piatok4->isChecked())
			zostatok = funkcia(19);

		odcheckovanie_checkBoxov();
		nacitanie_jedal();
		//zobrazenie.clear();	
	}
	
}

void jedalen::odhlasenie_klik()
{
	if (stav == true)
	{
		QMessageBox mbox;
		vymazanie_checkBoxov();
		QString nieco = "odhlasenie bolo uspesne.";
		zobrazenie.clear();
		mbox.setText(nieco);
		mbox.exec();
		ui.listWidget->clear();
		stav = false;
	}
}

void jedalen::dobi_klik()
{
	QMessageBox mbox;
	if (stav == true)
	{
		QString suma = ui.lineEdit->text();
		QString suma1 = suma.replace(",",".");
		double vysledok = kredity[index].toDouble() + suma1.toDouble();
		//ui.label->setText(QString::number(vysledok));
		int n = 0;

		QFile input("zoznam.txt");
		if (input.open(QFile::ReadOnly | QFile::Text))
		{
			QTextStream in(&input);
			while (!input.atEnd())
			{
				QString riadok = input.readLine();
				pomocna.push_back(riadok);
				n++;
			}

			input.close();

			for (int i = 0; i < n-2; i++)
			{
				int x = QString::compare(pomocna[i], mena[index], Qt::CaseSensitive);
				if (x == 1)
				{
					pomocna[i+3] = (QString::number(vysledok) + "\n");
					ui.kredit->setText("kredit: " + QString::number(vysledok));
				}
			}
		}

		QFile output("zoznam.txt");
		if (output.open(QFile::WriteOnly | QFile::Text))
		{
			QTextStream out(&output);
			for(int i = 0; i < n; i++)
				out << pomocna[i];
			output.close();
		}
		kredity[index] = QString::number(vysledok);
	}
	else
	{
		mbox.setText("Aby ste mohli dobit kredit musite byt prihlaseny.");
		mbox.exec();
		vymazanie_checkBoxov();
	}
}

void jedalen::zmaz()
{
	QMessageBox mbox;
	double vysledok;
	double pomoc;

	if (stav == true)
	{
		int j = ui.listWidget->currentRow();
		int pomooc = 0;
		
		for (int i = 0; i < jedla.size(); i++)
		{
			int x = QString::compare(zobrazenie[j], jedla[i], Qt::CaseSensitive); 
			if(x == 0)
				vysledok = kredity[index].toDouble() + cena[i].toDouble();
		}
		
		ui.kredit->setText("kredit: " + QString::number(vysledok));
		kredity[index] = QString::number(vysledok);

		QFile output("zoznam.txt");
		if (output.open(QFile::WriteOnly | QFile::Text))
		{
			QTextStream out(&output);
			for (int i = 0; i < mena.size(); i++)
			{
				out << mena[i] << endl;
				out << hesla[i] << endl;
				out << pohlavia[i] << endl;
				out << kredity[i] << endl;
			}
			output.close();
		}

		zobrazenie.removeAt(j);
		pomooc = zobrazenie.size();


		QFile output1(mena[index] + ".txt");
		if (output1.open(QFile::WriteOnly | QFile::Text | QFile::Truncate))
		{
			QTextStream out(&output1);
			for (int i = 0; i < pomooc; i++)
			{
				out << zobrazenie[i] << endl;
			}
			output1.close();
		}
		ui.listWidget->takeItem(j);
		pomooc = 0;
	}
	else
	{
		mbox.setText("nemozete vymazat obed nie ste prihlaseny.");
		mbox.exec();
	}
}